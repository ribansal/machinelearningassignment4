import gym
import value_iteration
import policy_iteration

env = gym.make('FrozenLake-v0')
env.reset()
print("Action space: ", env.action_space)
print("Observation space: ", env.observation_space)
env.render()

#running value iteration
df =0.9
VI = value_iteration.ValueIterationAlgo(env, theta = 0.00001, discount_factor=df)
p, v, iterations, time_per_iteration, deltaiter, rewarditer = VI.run_algo()
print("number of iterations = ", iterations)
print("values are ",v)
print("policy is ", p)
VI.plot_VI()

"""
#running policy iteraiion
df = 0.9
PI = policy_iteration.PolicyIterationAlgo(env,theta = 0.00001,discount_factor=df)
p, v, iterations = PI.run_algo()
print("number of iterations = ", iterations)
print("policy is ", p)
"""

