import os, sys
import plot
import pandas as pd
import numpy as np
import value_iteration 
import policy_iteration 
import q_learning
import plotting

def run_valueiteration_algorithm(env,size):

	discount_factor = [0.1, 0.3, 0.5, 0.7, 0.9]
	VIresults = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	for df in discount_factor:
		print("results for value iterations at discount factor %s"%df)
		VI = value_iteration.ValueIterationAlgo(env, theta = 1.0E-5, discount_factor=df,max_attempts=10)
		p, v, iterations, time_per_iteration, deltaiter, rewarditer = VI.run_algo()
		dt = {'iterations':np.arange(1,iterations+1), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':VI.policyloss}
		#computing converged policy delta
		dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
		dfresults["finaldelta"] = deltaiter[-1]
		dfresults["iterations"] = iterations
		dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
		dfresults["reward"] = rewarditer[-1]
		policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 

		vi_df = pd.DataFrame(dt)
		VIresults[df] = vi_df
		#print(vi_df.head())
		csv_name = "VI_frozenlake_df_" + str(df) + ".csv"
		vi_df.to_csv(csv_name)
		print("number of iterations = ", iterations)
		#print("values are ",v)
		#print("policy are ",p)
		policy_plot_name ="VI_frozenlake_df_" + str(df) + "PolicyPlot" 
		plot.plot_VI(VI.env,VI.V,VI.policy,size,policy_plot_name)

	plot.plot_convergence(VIresults,policy_convergence,"value_small")
	policy_convergence.to_csv("valuerun.log",sep='\t',float_format='%.8f')


def run_policyiteration_algorithm(env,size):

	discount_factor = [0.1, 0.3, 0.5, 0.7, 0.9]
	VIresults = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	for df in discount_factor:
		print("results for value iterations at discount factor %s"%df)
		PI = policy_iteration.PolicyIterationAlgo(env, theta = 1.0E-5, discount_factor=df,max_attempts=10)
		p, v, iterations, time_per_iteration, deltaiter, rewarditer = PI.run_algo()
		dt = {'iterations':np.arange(1,iterations+1), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':PI.policyloss}
		
		#computing converged policy delta
		dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
		dfresults["finaldelta"] = deltaiter[-1]
		dfresults["iterations"] = iterations
		dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
		dfresults["reward"] = rewarditer[-1]
		policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 

		vi_df = pd.DataFrame(dt)
		VIresults[df] = vi_df
		#print(vi_df.head())
		csv_name = "PI_frozenlake_df_" + str(df) + ".csv"
		vi_df.to_csv(csv_name)
		print("number of iterations = ", iterations)
		#print("values are ",v)
		#print("policy are ",p)
		policy_plot_name ="PI_frozenlake_df_" + str(df) + "PolicyPlot" 
		plot.plot_VI(PI.env,PI.V,PI.policy,size,policy_plot_name)

	plot.plot_convergence(VIresults,policy_convergence,"policy_small")
	policy_convergence.to_csv("policyrun.log",sep='\t',float_format='%.8f')



def run_qlearniteration_algorithm(env,size):

	discount_factor = [0.1, 0.3, 0.5, 0.7, 0.9]
	VIresults = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	if size < 8:
		attempts = 100
		window = 5
		eps = 0.8
		epsdecay = 0.001
	else :
		attempts = 100
		window = 20
		eps = 0.8
		epsdecay = 1.0E-3

	for df in discount_factor:
		print("results for value iterations at discount factor %s"%df)
		Ql = q_learning.QLearningAlgo(env,theta = 1.0E-5, alpha = 0.8, discount_factor = df, epsilon = eps, num_episodes = 120000, decay = epsdecay, max_attempts=attempts )
		#p, v, iterations, time_per_iteration, deltaiter, rewarditer = QL.run_algo()
		Q, stats = Ql.run_algo()
		p = Ql.policy
		v = Ql.V
		iterations = Ql.n_iterations
		deltaiter = Ql.deltaiter
		time_per_iteration = Ql.time_per_iteration
		rewarditer = Ql.rewarditer
		dt = {'iterations':np.arange(1,iterations+1), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':Ql.policyloss,'episodelength':stats.episode_lengths,'episodereward':stats.episode_rewards}
		
		#computing converged policy delta
		dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
		dfresults["finaldelta"] = deltaiter[-1]
		dfresults["iterations"] = iterations
		dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
		dfresults["reward"] = rewarditer[-1]
		dfresults["episodelength"] = stats.episode_lengths[-1]
		dfresults["episodereward"] = stats.episode_rewards[-1]
		dfresults["policyloss"] = Ql.policyloss[-1]
		policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 
		vi_df = pd.DataFrame(dt)
		VIresults[df] = vi_df
		#print(vi_df.head())
		csv_name = "Ql_frozenlake_df_" + str(df) + ".csv"
		vi_df.to_csv(csv_name)
		print("number of iterations = ", iterations)
		#print("values are ",v)
		#print("policy are ",p)
		#plotting.plot_episode_stats(stats,"df-%s"%df)
		policy_plot_name ="Ql_frozenlake_df_" + str(df) + "PolicyPlot" 
		plot.plot_VI(Ql.env,Ql.V,Ql.policy,size,policy_plot_name)

	plot.plot_qlearn_convergence(VIresults,policy_convergence,"qlearm_small",window)
	policy_convergence.to_csv("qlearnrun.log",sep='\t',float_format='%.8f')

def run_tuneqlearniteration_algorithm(env,size):

	discount_factor = [0.9]
	epslist = [0.5]
	epsdecaylist = [0.001]
	alphalist = [0.1, 0.2, 0.4, 0.6, 0.8]

	VIresults = {}
	iterresults = {}
	iter_policy_results = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	if size < 8:
		attempts = 100
		window = 5
	else :
		attempts = 100
		window = 5

	for df in discount_factor:
		for alphai in alphalist:
			for eps in epslist:
				for epsdecay in epsdecaylist:
					VIresults = {}
					print("results for value iterations at discount factor %s"%df)
					Ql = q_learning.QLearningAlgo(env,theta = 1.0E-5, alpha = alphai, discount_factor = df, epsilon = eps, num_episodes = 120000, decay = epsdecay, max_attempts=attempts )
					#p, v, iterations, time_per_iteration, deltaiter, rewarditer = QL.run_algo()
					Q, stats = Ql.run_algo()
					p = Ql.policy
					v = Ql.V
					iterations = Ql.n_iterations
					deltaiter = Ql.deltaiter
					time_per_iteration = Ql.time_per_iteration
					rewarditer = Ql.rewarditer
					dt = {'iterations':np.arange(1,iterations+1), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':Ql.policyloss,'episodelength':stats.episode_lengths,'episodereward':stats.episode_rewards}
		
					#computing converged policy delta
					dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
					dfresults['alpha'] = alphai
					dfresults["eps"] = eps
					dfresults['decay'] = epsdecay
					dfresults["finaldelta"] = deltaiter[-1]
					dfresults["iterations"] = iterations
					dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
					dfresults["reward"] = rewarditer[-1]
					dfresults["policyloss"] = Ql.policyloss[-1]
					dfresults["episodelength"] = stats.episode_lengths[-1]
					dfresults["episodereward"] = stats.episode_rewards[-1]
					policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 
					vi_df = pd.DataFrame(dt)
					VIresults[df] = vi_df
					iterresults[df,alphai,eps,epsdecay] = vi_df
					#prefix = "qlearn-df-%s-alpha-%s-eps-%s-decay-%s"%(df,alphai,eps,epsdecay)
					#plot.plot_tuneqlearn_convergence(VIresults,prefix,window)
	policy_convergence.to_csv("qlearnrun.log",sep='\t',float_format='%.8f')

	#plotting results for one variable keeping other two constant
	
	for df in discount_factor:
			for eps in epslist:
				for epsdecay in epsdecaylist:
					results = {}
					for alphai in alphalist:
						results[alphai] = iterresults[df,alphai,eps,epsdecay]
					df1 = policy_convergence[(policy_convergence.df == df)]
					df1 = df1[(df1.eps == eps)]
					df1 = df1[(df1.decay == epsdecay)]
					pretitle = "df=%s,eps=%s,epsdecay=%s"%(df,eps,epsdecay)
					plot.plot_tuneqlearn_convergence(results,df1,pretitle,"alpha",window=30)
	

	"""
	for df in discount_factor:
		for alphai in alphalist:
				for epsdecay in epsdecaylist:
					results = {}
					for eps in epslist:
						results[eps] = iterresults[df,alphai,eps,epsdecay]
					df1 = policy_convergence[(policy_convergence.df == df)]
					df1 = df1[(df1.alpha == alphai)]
					df1 = df1[(df1.decay == epsdecay)]
					pretitle = "df=%s,alpha=%s,epsdecay=%s"%(df,alphai,epsdecay)
					plot.plot_tuneqlearn_convergence(results,df1,pretitle,"eps",window=10)
	"""

	"""
	for df in discount_factor:
		for alphai in alphalist:
			for eps in epslist:
					results = {}
					for epsdecay in epsdecaylist:
						results[epsdecay] = iterresults[df,alphai,eps,epsdecay]
					df1 = policy_convergence[(policy_convergence.df == df)]
					df1 = df1[(df1.alpha == alphai)]
					df1 = df1[(df1.eps == eps)]
					pretitle = "df=%s,alpha=%s,eps=%s"%(df,alphai,eps)
					plot.plot_tuneqlearn_convergence(results,df1,pretitle,"decay",window=10)
	"""


def compute_rmse_matrix(A,B):
	loss = A-B
	n = len(A)
	#loss = sum(sum(abs(loss)))
	loss = np.sqrt((sum(sum(loss**2)))) 
	return loss

def compute_qloss(A,B):
	loss = A-B
	n = len(A)
	loss = sum(sum(abs(loss))) 
	return loss
