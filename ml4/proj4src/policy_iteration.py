import numpy as np
import sys
from base_algo import BaseAlgo
import time
import copy
import algorithm

class PolicyIterationAlgo(BaseAlgo):
    def __init__(self, env,theta = 0.00001,discount_factor = 1.0,max_attempts=10):
        self.env = env
        #self.V = np.zeros(self.env.nS)
        self.theta = theta
        self.discount_factor = discount_factor
        self.policy = np.ones([self.env.nS, self.env.nA]) / self.env.nA
        self.n_iterations = 0
        self.delta = 0.0
        self.max_iter = 500

        #list for iterations
        self.Viter = []
        self.Piter = []
        self.Vsum = []
        self.rewarditer = []
        self.deltaiter = []
        self.time_per_iteration = []
        self.policyloss = []
        self.max_attempts = max_attempts

    def get_env(self):
        return self.env

    def calc_action_state(self,env, state, V,discount_factor):
        actions = np.zeros(env.nA)
        for i in range(env.nA):
            for p, next_state, reward, done in env.P[state][i]:
                actions[i] += p *(reward + discount_factor * V[next_state])
        return actions

    def policy_evaluation(self):
        V = np.zeros(self.env.nS)
        while True:
            delta = 0
            for state in range(self.env.nS):
                v=0
                oldV = V[state]
                for a , a_prob in enumerate(self.policy[state]):
                    for prob, next_state, reward, done in self.env.P[state][a]:
                        v+= a_prob * prob * (reward + self.discount_factor * V[next_state])

                #delta = max(delta, np.abs(oldV - self.V[state]))
                delta = max(delta, np.abs(oldV - v))
                V[state] = v
            if delta < self.theta:
                break
        return np.array(V)

    def run_algo(self):
        self.policy = np.ones([self.env.nS, self.env.nA]) / self.env.nA
        prevdelta = 0
        attempt = 0
        oldpolicy = np.ones([self.env.nS, self.env.nA])/self.env.nA

        for i in range(self.max_iter):
            delta = 0
            reward = 0
            start_time = time.perf_counter()

            V = self.policy_evaluation()
            policy_stable = True

            for s in range(self.env.nS):
                chosen_action = np.where( self.policy[s] == self.policy[s].max())
                #chosen_action = np.argmax(self.policy[s])

                action_values = self.calc_action_state(self.env,s,V, self.discount_factor)
                #best_action = np.argmax(action_values)
                best_action_value = max(action_values)
                best_action = np.where( action_values == action_values.max())

                #if chosen_action != best_action:
                if not np.array_equal(chosen_action,best_action):
                    policy_stable = False

                delta = max(delta, np.abs(best_action_value - V[s]))
                reward += best_action_value

                self.policy[s] = np.zeros(self.env.nA)
                for ids in best_action:
                    self.policy[s][ids] = 1.0/(len(best_action[0]))
                #self.policy[s][best_action] = 1.0
            
            self.n_iterations += 1
            self.time_per_iteration.append(time.perf_counter() - start_time)
            self.deltaiter.append(delta)
            self.rewarditer.append(reward)
            policyloss = self.policy - oldpolicy
            #self.policyloss.append(sum(sum(np.sqrt(policyloss**2))))
            self.policyloss.append(algorithm.compute_rmse_matrix(self.policy,oldpolicy))
            oldpolicy = copy.copy(self.policy)

            #if policy_stable:
            if delta < self.theta:
                attempt += 1
            else:
                attempt = 0

            if attempt >= self.max_attempts:
                print("number of iterations for convergence", self.n_iterations)
                break 

        self.V = V
        
        return self.policy, self.V, self.n_iterations, self.time_per_iteration, self.deltaiter, self.rewarditer