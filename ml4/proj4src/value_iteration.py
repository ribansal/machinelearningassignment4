import numpy as np
import sys
from base_algo import BaseAlgo
from matplotlib import pyplot as plt
import copy
import time
import algorithm

class ValueIterationAlgo(BaseAlgo):
    def __init__(self, env, theta = 0.00001, discount_factor = 1.0, max_attempts=10):
        self.env = env
        self.V = np.zeros(self.env.nS)

        self.theta = theta
        self.discount_factor = discount_factor
        self.policy = np.zeros([self.env.nS, self.env.nA])
        self.n_iterations = 0
        self.delta = 0.0
        #list for iterations
        self.Viter = []
        self.Piter = []
        self.Vsum = []
        self.rewarditer = []
        self.deltaiter = []
        self.max_iter = 500
        self.time_per_iteration = []
        self.policyloss = []
        self.max_attempts = max_attempts

    def get_env(self):
        return self.env

    def calc_action_state(self,env, state, V,discount_factor):
        actions = np.zeros(env.nA)
        for i in range(env.nA):
            for p, next_state, reward, done in env.P[state][i]:
                actions[i] += p *(reward + discount_factor * V[next_state])
        return actions

    def run_algo(self):

        policydelta = 0.0
        attempt = 0
        oldpolicy = np.ones([self.env.nS, self.env.nA])/self.env.nA

        for i in range(self.max_iter):
            start_time = time.perf_counter()
            delta = 0
            reward = 0
            iterpolicy = np.zeros([self.env.nS, self.env.nA])
            for state in range(self.env.nS):
                actions = self.calc_action_state(self.env, state, self.V, self.discount_factor)
                oldV = self.V[state]
                self.V[state] = np.max(actions)
                delta = max(delta, np.abs(oldV - self.V[state]))
                reward += self.V[state]
            self.time_per_iteration.append(time.perf_counter() - start_time)

            for state in range(self.env.nS):
                best_action = self.calc_action_state(self.env, state, self.V, self.discount_factor)
                #best_action_index = np.argmax(best_action)
                indices = np.where( best_action == best_action.max())
                for ids in indices:
                    iterpolicy[state, ids] = 1.0/len(indices[0])

            self.Viter.append(copy.deepcopy(self.V))
            self.Piter.append(iterpolicy)
            self.Vsum.append(sum(self.V))
            self.rewarditer.append(reward)
            self.deltaiter.append(delta)
            self.n_iterations += 1

            policyloss = iterpolicy - oldpolicy
            #self.policyloss.append(sum(sum(np.sqrt(policyloss**2))))
            self.policyloss.append(algorithm.compute_rmse_matrix(iterpolicy,oldpolicy))
            oldpolicy = iterpolicy

            if delta < self.theta:
                attempt += 1
            else:
                attempt = 0

            if attempt >= self.max_attempts:
                print("number of iterations for convergence", self.n_iterations)
                break 
            

        self.policy = np.zeros([self.env.nS, self.env.nA])
        for state in range(self.env.nS):
            best_action = self.calc_action_state(self.env, state, self.V, self.discount_factor)
            best_action_index = np.argmax(best_action)
            indices = np.where( best_action == best_action.max())
            for ids in indices:
                self.policy[state, ids] = 1/len(indices[0])


        return self.policy,self.V, self.n_iterations, self.time_per_iteration, self.deltaiter, self.rewarditer






