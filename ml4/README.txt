Libraries used:
Python version - 3.7.4
scikit-learn version - 0.21.2
matplotlib version - 3.1.1
numpy version - 1.16.4
pandas version - 0.25.1
openai gym version 0.15.4

Please clone repository as follows:


Runfile information:

1. Frozen lake problem:
   Frozen lake problem is solved using entry file run_frozenlake.py.
   This file takes 2 argumnets:
   1st Argumnet is the size of the problem: Currently supported sizes are :
					4 for (4x4) problem size
					8 for (8x8) problem size
					20 for (20x20) problem size
					24 for (24x24) problem size
   2nd Argument is the type of method to solve this problem
                   value  : for solving problem with value iteration 
				   policy : for solving problem with policy iteration
				   qlearn : for solving problem with q-learning algorithm
   sample run example:
   python ./run_frozenlake.py 24 value

2. Tower of Hanoi problem:
   This problem is solved using entry file run_toh.py.
   Currently problem is set for solving a problem size of 4 disks. This entry file
   does not take any arguments and run all three algorithms.
   sample run example:
   python ./run_toh.py