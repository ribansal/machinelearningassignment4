import gym
from gym.envs.registration import register
from .frozen_lake import *
from .toh_env import *
__all__ = ["ModifiedFrozenLakeEnv", "TohEnv"]

register(
    id='ModifiedFrozenLake-v0',
    entry_point='environments:ModifiedFrozenLakeEnv',
    kwargs={'map_name': '4x4','rewarding':True,'is_slippery':False},
)

register(
    id='ModifiedFrozenLake-8x8-v0',
    entry_point='environments:ModifiedFrozenLakeEnv',
    kwargs={'map_name': '8x8','rewarding':True,'is_slippery':False},
)

register(
    id='ModifiedFrozenLake-20x20-v0',
    entry_point='environments:ModifiedFrozenLakeEnv',
    kwargs={'map_name': '20x20','rewarding':True,'is_slippery':False},
)

register(
    id='ModifiedFrozenLake-24x24-v0',
    entry_point='environments:ModifiedFrozenLakeEnv',
    kwargs={'map_name': '24x24','rewarding':True,'is_slippery':False},
)

register(
    id='TohEnv-v0',
    entry_point='environments:TohEnv',
    #kwargs={'map_name': '24x24'},
)

register(
    id='TohEnv-4-v0',
    entry_point='environments:TohEnv',
    kwargs={'initial_state': ((3, 2, 1, 0), (), ()), 'goal_state':((), (), (3, 2, 1, 0))}
)

def make_4x4_frozen_lake_env():
    return gym.make('ModifiedFrozenLake-v0')

def make_8x8_frozen_lake_env():
    return gym.make('ModifiedFrozenLake-8x8-v0')

def make_20x20_frozen_lake_env():
    return gym.make('ModifiedFrozenLake-20x20-v0')

def make_24x24_frozen_lake_env():
    return gym.make('ModifiedFrozenLake-24x24-v0')

def generate_frozen_lake(s):
    my_frozen_lake =flake.generate_random_map(size=s)
    filename = "frozen_lake_" + str(s) + ".txt"
    f = open(filename, "w")
    f.write(str(my_frozen_lake))
    f.close()

def make_small_toh_env():
    return gym.make('TohEnv-v0')
def make_toh_4_env():
    return gym.make('TohEnv-4-v0')
