from matplotlib import pyplot as plt
import numpy as np

colormap = {'H' : 'red','S':'blue','F':'black','G':'green'}

def plot_VI(env,V,policy,size,figname):
    Vlist = [V]
    Plist = [policy]
    #print(env.P[0])
    for (V, pi) in zip(Vlist, Plist):
        pi = convert_policy_action_vector(pi)
        fig = plt.figure(figsize=(6,6))
        plt.imshow(V.reshape(size,size), cmap='Blues', clim=(0.5,0.5))

        ax = plt.gca()
        ax.set_xticks(np.arange(size+1)-.5)
        ax.set_yticks(np.arange(size+1)-.5)
        ax.set_xticklabels(list(np.arange(size+1)))
        ax.set_yticklabels(list(np.arange(size+1)))
        Y, X = np.mgrid[0:size, 0:size]
        a2uv = {0: (-1, 0), 1:(0, -1), 2:(1,0), 3:(0, 1)}
        Pi = pi.reshape(size,size)
        for y in range(size):
            for x in range(size):
                a = Pi[y, x]
                if not isinstance(a,np.ndarray): a = [a]
                for ai in a:
                	u, v = a2uv[ai]
                	stype = str(env.unwrapped.desc[y,x].item().decode())
                	if stype not in "HG":
                		plt.arrow(x, y,u*.3, -v*.3, color='g', head_width=0.25, head_length=0.25) 
                	plt.text(x, y, stype, size=9,  verticalalignment='center',
                        horizontalalignment='center', fontweight='bold',color=colormap[stype])
        plt.grid(color='b', lw=2, ls='-')
        
        plt.savefig("%s.png"%figname,dpi=150, bbox_inches='tight')
        fig.clear()

def convert_policy_action_vector(pi):
    pa = []
    for ai in pi:
    	ids = np.where(ai != 0.0)
    	pa.append(tuple(ids))
    return np.array(pa)


def plot_delta(df, title, xlabel, ylabel, xname, yname, figname):
    fig = plt.figure()
    plt.title(title)
    plt.plot(df[xname], df[yname], linewidth=1,marker='s',markersize=3,alpha=0.8)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    #plt.legend(loc='best')
    plt.grid()
    plt.savefig("%s.png"%figname,dpi=150, bbox_inches='tight')
    plt.close(fig)


def plot_discount_convergence(results,title, xlabel, ylabel, xname, yname, figname):

	fig = plt.figure()
	plt.title(title)
	for df in results:
		ri = results[df]
		plt.plot(ri[xname], ri[yname], marker='s',markersize=3,linewidth=2,label="discount factor %s"%df, alpha=0.8)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.legend(loc='best')
	plt.grid()
	plt.savefig("%s.png"%figname,dpi=150, bbox_inches='tight')
	plt.close(fig)

def plot_logdiscount_convergence(results,title, xlabel, ylabel, xname, yname, figname):

	fig = plt.figure()
	plt.title(title)
	for df in results:
		ri = results[df]
		smoothy = ri[yname].rolling(50).mean()
		plt.loglog(ri[xname], smoothy, marker='s',markersize=5,linewidth=1,label="discount factor %s"%df)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.legend(loc='best')
	plt.grid()
	plt.savefig("%s.png"%figname,dpi=150, bbox_inches='tight')
	plt.close(fig)

def plot_smoothdiscount_convergence(results,title, xlabel, ylabel, xname, yname, figname,window=10):

	fig = plt.figure()
	plt.title(title)
	for df in results:
		ri = results[df]
		smoothy = ri[yname].rolling(window).mean()
		plt.plot(ri[xname], smoothy, linewidth=1,label="discount factor %s"%df)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.legend(loc='best')
	plt.grid()
	plt.savefig("%s.png"%figname,dpi=150, bbox_inches='tight')
	plt.close(fig)

def apply_action_TOH(s, a):
    """Apply a move and generate the new state, if move is invalid, return None."""
    s = [list(i) for i in s]
    if len(s[a[0]]) == 0:
        # Invalid move
        return None
    to_move = s[a[0]][-1]
    source = a[0]
    dest = a[1]
    new_state = s[:]
    new_state[source].pop()
    new_state[dest].append(to_move)
    output = tuple([tuple(i) for i in new_state])
    return output

def plot_policy_TOH(all_states, all_actions, policy, filename):
    fpolicy = {}
    for i, j in zip(policy, all_states):

        i = i.tolist()
        if 1 in i:
            index1 = i.index(1)
            action = all_actions[index1]
            final_state = apply_action_TOH(j, action)
            fpolicy[j] = final_state
        else:
            fpolicy[j] = -1
    filename2 = filename + ".txt"
    fp = open(filename2, 'w')
    
    for x,y in fpolicy.items():
        line = str(x) + '->' + str(y) + "\n"
        fp.write(line)
    fp.close()
    #filename3 = filename + "_stdiagram.png"
    #stdiag.make_stdiagram(fpolicy, filename3)
        

def plot_convergence(VIresults,policy_convergence,prefix):
	plot_discount_convergence(VIresults,'Delta Convergence','Steps','Delta','iterations','delta', '%s_delta_convergence'%prefix)
	plot_discount_convergence(VIresults,'Rewards Convergence','Steps','Rewards','iterations','reward', '%s_reward_convergence'%prefix)
	plot_discount_convergence(VIresults,'Total time vs iterations','Steps','Time(s)','iterations','time_per_iteration', '%s_time_convergence'%prefix)
	plot_discount_convergence(VIresults,'Policy loss vs iterations','Steps','Policy loss','iterations','policyloss', '%s_policyloss_convergence'%prefix)
	plot_delta(policy_convergence, 'Policy Convergence', 'Discount factor', 'Policy delta','df','policy',"%s_policy_convergence"%prefix)
	plot_delta(policy_convergence, 'Iterations vs Discount Factor', 'Discount factor', 'Number of iterations','df','iterations',"%s_iterations_convergence"%prefix)


def plot_qlearn_convergence(VIresults,policy_convergence,prefix,window=10):
	plot_smoothdiscount_convergence(VIresults,'Delta Convergence','Steps','Delta','iterations','delta', '%s_delta_convergence'%prefix,window)
	plot_smoothdiscount_convergence(VIresults,'Rewards Convergence','Steps','Rewards','iterations','reward', '%s_reward_convergence'%prefix,window)
	plot_smoothdiscount_convergence(VIresults,'Total time vs iterations','Steps','Time(s)','iterations','time_per_iteration', '%s_time_convergence'%prefix,window)
	plot_smoothdiscount_convergence(VIresults,'Policy loss vs iterations','Steps','Policy loss','iterations','policyloss', '%s_policyloss_convergence'%prefix,window)
	plot_smoothdiscount_convergence(VIresults,'Episode Length vs Steps','Steps','Episode Length','iterations','episodelength', '%s_episodelength'%prefix,window)
	plot_smoothdiscount_convergence(VIresults,'Episode Rewards vs Steps','Steps','Episode Rewards','iterations','episodereward', '%s_episoderewards'%prefix,window)
	plot_delta(policy_convergence, 'Policy Convergence', 'Discount factor', 'Policy delta','df','policy',"%s_policy_convergence"%prefix)
	plot_delta(policy_convergence, 'Iterations vs Discount Factor', 'Discount factor', 'Number of iterations','df','iterations',"%s_iterations_convergence"%prefix)

