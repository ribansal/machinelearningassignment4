import numpy as np
from abc import ABC, abstractmethod

class BaseAlgo(ABC):
    def __init__(self):
        pass
    @abstractmethod
    def run_algo(self):
        pass
    def run_until_converged(self):
        pass
    def get_env(self):
        pass
    
