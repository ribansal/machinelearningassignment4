import numpy as np
import sys
import itertools
import matplotlib
import pandas as pd
from base_algo import BaseAlgo
import plotting
from collections import defaultdict
import copy
import time
import algorithm
matplotlib.style.use('ggplot')

class QLearningAlgo(BaseAlgo):
    def __init__(self, env,theta = 0.00001,alpha = 0.5, discount_factor = 1.0, epsilon = 0.1, num_episodes = 1000, decay = 0.01, max_attempts = 10):
        self.env = env
        self.V = np.zeros(self.env.nS)
        self.theta = theta
        self.alpha = alpha
        self.discount_factor = discount_factor
        self.policy = np.ones([self.env.nS, self.env.nA]) / self.env.nA
        self.n_iterations = 0
        #self.Q = defaultdict(lambda: np.zeros(env.action_space.n))
        self.Q = np.zeros([self.env.nS, self.env.nA])
        self.epsilon = epsilon
        self.num_episodes = num_episodes
        self.decay = decay
        self.max_attempts = max_attempts
        self.rewarditer = []
        self.deltaiter = []
        self.time_per_iteration = []
        self.policyloss = []

    def get_env(self):
        return self.env
    
    def make_epsilon_greedy_policy(self, Q, epsilon, nA):
        def policy_fn(observation):
            A = np.ones(nA, dtype=float) * epsilon / nA
            #best_action = np.argmax(Q[observation])
            best_action = np.where( Q[observation] == Q[observation].max())
            for ids in best_action:
                A[ids] += (1.0 - epsilon)/(len(best_action[0]))
            #A[best_action] += (1.0 - epsilon)
            return A
        return policy_fn

    def greedy_policy(self, observation):
        nA = self.env.action_space.n
        A = np.ones(nA, dtype=float) * self.epsilon / nA
        #best_action = np.argmax(Q[observation])
        best_action = np.where( self.Q[observation] == self.Q[observation].max())
        for ids in best_action:
            A[ids] += (1.0 - self.epsilon)/(len(best_action[0]))
        #A[best_action] += (1.0 - epsilon)
        return A
    
    def run_algo(self):
        
        np.random.seed(0)
        stats = plotting.EpisodeStats(
        episode_lengths=np.zeros(self.num_episodes),
        episode_rewards=np.zeros(self.num_episodes))    

        #policy = self.make_epsilon_greedy_policy(self.Q, self.epsilon, self.env.action_space.n)
        attempt = 0
        oldpolicy = np.ones([self.env.nS, self.env.nA])/self.env.nA

        for i_episode in range(self.num_episodes):
            #policy = self.make_epsilon_greedy_policy(self.Q, self.epsilon, self.env.action_space.n)
            delta = 0
            
            if (i_episode + 1) % 100 == 0:
                print("\rEpisode {}/{}.".format(i_episode + 1, self.num_episodes), end="")
                sys.stdout.flush()
            start_time = time.perf_counter()
            state = self.env.reset()
            oldq = copy.copy(self.Q)

            for t in itertools.count():
                #action_probs = policy(state)
                action_probs = self.greedy_policy(state)

                action = np.random.choice(np.arange(len(action_probs)), p=action_probs)

                next_state, reward, done, _ = self.env.step(action)

                stats.episode_rewards[i_episode] += reward
                stats.episode_lengths[i_episode] = t

                # best_next_action = np.argmax(self.Q[next_state])    
                # td_target = reward + self.discount_factor * self.Q[next_state][best_next_action]
                # td_delta = td_target - self.Q[state][action]
                # self.Q[state][action] += self.alpha * td_delta
                # delta = max(delta, np.abs(td_delta))
                best_next_action = np.where( self.Q[next_state] == self.Q[next_state].max())
                for ids in best_next_action[0]:
                    td_target = reward + self.discount_factor * self.Q[next_state][ids]
                    td_delta = td_target - self.Q[state][action]
                    self.Q[state][action] += self.alpha * td_delta
                    delta = max(delta, np.abs(td_delta))
                    
                if done:
                    break
                    
                state = next_state

            #policy based on best Q values 
            self.policy = np.zeros([self.env.nS, self.env.nA])
            for state in range(self.env.nS):
                indices = np.where( self.Q[state] == self.Q[state].max())
                self.V[state] = self.Q[state].max()
                for ids in indices:
                    self.policy[state, ids] = 1/len(indices[0])  

            #applying eps decay
            self.epsilon *= self.decay
            self.deltaiter.append(delta)
            self.time_per_iteration.append(time.perf_counter() - start_time)
            self.rewarditer.append(sum(self.V))
            self.policyloss.append(algorithm.compute_rmse_matrix(self.policy,oldpolicy))
            oldpolicy = copy.copy(self.policy)
            #qerror = algorithm.compute_qloss(oldq,self.Q)
            
            if delta < self.theta:
            #if qerror < self.theta:
                attempt += 1
            else:
                attempt = 0
            self.n_iterations += 1
            if attempt >= self.max_attempts:
                print("number of episodes for convergence", i_episode)
                
                stats = plotting.EpisodeStats(
                        episode_lengths=stats.episode_lengths[0:i_episode+1],
                        episode_rewards=stats.episode_rewards[0:i_episode+1])
                break 
                

        print("final delta is", delta )

        return self.Q, stats