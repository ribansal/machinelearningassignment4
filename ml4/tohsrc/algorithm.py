import os, sys
import plot
import pandas as pd
import numpy as np
import value_iteration 
import policy_iteration 
import q_learning 
import plotting

def run_valueiteration_algorithm(env,size):

	discount_factor = [0.1, 0.3, 0.5, 0.7, 0.9]
	VIresults = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	for df in discount_factor:
		print("results for value iterations at discount factor %s"%df)
		VI = value_iteration.ValueIterationAlgo(env, theta = 1.0E-5, discount_factor=df,max_attempts=10)
		p, v, iterations, time_per_iteration, deltaiter, rewarditer = VI.run_algo()
		dt = {'iterations':np.arange(iterations), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':VI.policyloss}
		#computing converged policy delta
		dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
		dfresults["finaldelta"] = deltaiter[-1]
		dfresults["iterations"] = iterations
		dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
		dfresults["reward"] = rewarditer[-1]
		policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 

		vi_df = pd.DataFrame(dt)
		VIresults[df] = vi_df
		#print(vi_df.head())
		csv_name = "VI_frozenlake_df_" + str(df) + ".csv"
		vi_df.to_csv(csv_name)
		print("number of iterations = ", iterations)
		#print("values are ",v)
		#print("policy are ",p)
		policy_plot_name ="VI_frozenlake_df_" + str(df) + "PolicyPlot" 
		plot.plot_VI(VI.env,VI.V,VI.policy,size,policy_plot_name)

	plot.plot_convergence(VIresults,policy_convergence,"value_small")
	policy_convergence.to_csv("valuerun.log",sep='\t',float_format='%.8f')


def run_policyiteration_algorithm(env,size):

	discount_factor = [0.1, 0.3, 0.5, 0.7, 0.9]
	VIresults = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	for df in discount_factor:
		print("results for value iterations at discount factor %s"%df)
		PI = policy_iteration.PolicyIterationAlgo(env, theta = 1.0E-5, discount_factor=df,max_attempts=10)
		p, v, iterations, time_per_iteration, deltaiter, rewarditer = PI.run_algo()
		dt = {'iterations':np.arange(iterations), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':PI.policyloss}
		
		#computing converged policy delta
		dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
		dfresults["finaldelta"] = deltaiter[-1]
		dfresults["iterations"] = iterations
		dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
		dfresults["reward"] = rewarditer[-1]
		policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 

		vi_df = pd.DataFrame(dt)
		VIresults[df] = vi_df
		#print(vi_df.head())
		csv_name = "PI_frozenlake_df_" + str(df) + ".csv"
		vi_df.to_csv(csv_name)
		print("number of iterations = ", iterations)
		#print("values are ",v)
		#print("policy are ",p)
		policy_plot_name ="PI_frozenlake_df_" + str(df) + "PolicyPlot" 
		plot.plot_VI(PI.env,PI.V,PI.policy,size,policy_plot_name)

	plot.plot_convergence(VIresults,policy_convergence,"policy_small")
	policy_convergence.to_csv("policyrun.log",sep='\t',float_format='%.8f')



def run_qlearniteration_algorithm(env,size):

	discount_factor = [0.1, 0.3, 0.5, 0.7, 0.9]
	VIresults = {}
	refpolicy = np.ones([env.nS, env.nA])/env.nA
	policy_convergence = pd.DataFrame()
	print(discount_factor)
	if size < 8:
		attempts = 100
		window = 5
		eps = 0.4
		epsdecay = 0.01
	else :
		attempts = 100
		window = 5
		eps = 0.4
		epsdecay = 1.0E-3

	for df in discount_factor:
		print("results for value iterations at discount factor %s"%df)
		Ql = q_learning.QLearningAlgo(env,theta = 1.0E-5, alpha = 0.5, discount_factor = df, epsilon = eps, num_episodes = 120000, decay = epsdecay, max_attempts=attempts )
		#p, v, iterations, time_per_iteration, deltaiter, rewarditer = QL.run_algo()
		Q, stats = Ql.run_algo()
		p = Ql.policy
		v = Ql.V
		iterations = Ql.n_iterations
		deltaiter = Ql.deltaiter
		time_per_iteration = Ql.time_per_iteration
		rewarditer = Ql.rewarditer
		dt = {'iterations':np.arange(iterations), 'time_per_iteration':np.cumsum(time_per_iteration), 'delta':deltaiter, 'reward':rewarditer,'policyloss':Ql.policyloss,'episodelength':stats.episode_lengths,'episodereward':stats.episode_rewards}
		
		#computing converged policy delta
		dfresults = {'df':df,'policy':compute_rmse_matrix(p,refpolicy)}
		dfresults["finaldelta"] = deltaiter[-1]
		dfresults["iterations"] = iterations
		dfresults["runtime"] = np.cumsum(time_per_iteration)[-1]
		dfresults["reward"] = rewarditer[-1]
		policy_convergence = policy_convergence.append(dfresults,ignore_index=True) 
		vi_df = pd.DataFrame(dt)
		VIresults[df] = vi_df
		#print(vi_df.head())
		csv_name = "Ql_frozenlake_df_" + str(df) + ".csv"
		vi_df.to_csv(csv_name)
		print("number of iterations = ", iterations)
		#print("values are ",v)
		#print("policy are ",p)
		#plotting.plot_episode_stats(stats,"df-%s"%df)
		policy_plot_name ="Ql_frozenlake_df_" + str(df) + "PolicyPlot" 
		plot.plot_VI(Ql.env,Ql.V,Ql.policy,size,policy_plot_name)

	plot.plot_qlearn_convergence(VIresults,policy_convergence,"qlearm_small",window)
	policy_convergence.to_csv("qlearnrun.log",sep='\t',float_format='%.8f')


def compute_rmse_matrix(A,B):
	loss = A-B
	n = len(A)
	#loss = sum(sum(abs(loss)))
	loss = np.sqrt((sum(sum(loss**2)))/n) 
	return loss

def compute_qloss(A,B):
	loss = A-B
	n = len(A)
	loss = sum(sum(abs(loss))) 
	return loss
