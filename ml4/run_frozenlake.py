import gym
import os, sys
path = os.path.join(sys.path[0],'proj4src')
sys.path.append(path)
import proj4src.value_iteration as value_iteration
import proj4src.policy_iteration as policy_iteration
import proj4src.q_learning as q_learning
import proj4src.plotting as plotting
import environments
import plot
import pandas as pd
import numpy as np
import algorithm
import copy

def main(ptype):

	method = sys.argv[2] #value policy qlearn
	tune = False
	#if sys.argv[3]=='yes':
	#	tune = True
	run_case(int(sys.argv[1]),method,tune)


def set_env_problem(size):
	if size==4:
		env = environments.make_4x4_frozen_lake_env()
	if size ==8 :
		env = environments.make_8x8_frozen_lake_env()
	if size == 20 :
		env = environments.make_20x20_frozen_lake_env()
	if size == 24 :
		env = environments.make_24x24_frozen_lake_env()
	return env

def run_case(probsize,method,tune):
	
	run_value = False
	run_policy = False
	run_qlearn = False
	run_qtune = False

	if method == 'value':
		run_value = True
	if method == 'policy' :
		run_policy = True
	if method == 'qlearn' :
		run_qlearn = True
		if tune :
			run_qtune = True

	size = probsize
	env = set_env_problem(size)

	env.reset()
	print("Action space: ", env.action_space)
	print("Observation space: ", env.observation_space)
	print("Observation space: ", env.observation_space.shape)
	print(env.isd)
	env.render()

	if run_value:
		#running value iteration
		rundir = "size_%d_value_frozenlake_small_run"%size
		os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
		os.chdir("%s"%rundir)
		#running value iteration
		algorithm.run_valueiteration_algorithm(copy.copy(env),size)
		os.chdir("../")

	if run_policy:
		#running policy iteration
		rundir = "size_%d_policy_frozenlake_small_run"%size
		os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
		os.chdir("%s"%rundir)
		#running value iteration
		algorithm.run_policyiteration_algorithm(copy.copy(env),size)
		os.chdir("../")
	
	if run_qlearn:
		#running policy iteration
		rundir = "size_%d_qlearn_frozenlake_small_run"%size
		os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
		os.chdir("%s"%rundir)
		#running value iteration
		if run_qtune:
			algorithm.run_tuneqlearniteration_algorithm(copy.copy(env),size)
		else:
			algorithm.run_qlearniteration_algorithm(copy.copy(env),size)
		os.chdir("../")



if __name__ == '__main__':
	ptype = sys.argv[1] #size of problem of frozen lake 4,8,20,24
	method = sys.argv[2] # value policy qlearn
	main(ptype)


