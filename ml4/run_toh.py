import gym
import os, sys
path = os.path.join(sys.path[0],'tohsrc')
sys.path.append(path)
import tohsrc.value_iteration as value_iteration
import tohsrc.policy_iteration as policy_iteration
import tohsrc.q_learning as q_learning
import tohsrc.plotting as plotting
import environments
import plot
import pandas as pd
import numpy as np
import algorithm_toh as algorithm
import copy

def main():
	run_small_case()

def plot_policy(all_states, policy):
    fpolicy = {}
    for i in policy:

        i = i.tolist()
        if 1 in i:
            index1 = i.index(1)
            print(index1)
            
    

def run_small_case():
    run_value = True
    run_policy = True
    run_qlearn = True

    #env = gym.make('FrozenLake-v0')
    #env = environments.make_small_toh_env()
    env = environments.make_toh_4_env()
    env.reset()
    print("Action space: ", env.action_space)
    print(env.nS)
    print(env.nA)

    all_states = env.all_states
    all_actions = env.action_list
    size = len(all_states)
    if run_value:
        #running value iteration
        rundir = "value_toh_small_size_%d"%size
        os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
        os.chdir("%s"%rundir)
        #running value iteration
        algorithm.run_valueiteration_algorithm(copy.copy(env),"toh", size, False,2)
        os.chdir("../")

    if run_policy:
        #running policy iteration
        rundir = "policy_toh_small_size_%d"%size
        os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
        os.chdir("%s"%rundir)
        #running value iteration
        algorithm.run_policyiteration_algorithm(copy.copy(env),"toh", size, False,2)
        os.chdir("../")
	
    if run_qlearn:
        #running policy iteration
        rundir = "qlearn_toh_size_%d"%size
        os.system("rm -rf %s && mkdir %s"%(rundir,rundir))
        os.chdir("%s"%rundir)
        #running value iteration
        algorithm.run_qlearniteration_algorithm(copy.copy(env),"toh", size, False,20)
        os.chdir("../")


if __name__ == '__main__':
	#ptype = sys.argv[1] 
	main()
